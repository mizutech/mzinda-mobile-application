package com.mhubmw.mzinda.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.Utils.Constants;
import com.mhubmw.mzinda.Utils.UrlEndpoints;
import com.mhubmw.mzinda.Utils.Utilities;
import com.mhubmw.mzinda.models.PollAnswers;
import com.mhubmw.mzinda.models.Polls;
import com.mhubmw.mzinda.network.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.mhubmw.mzinda.Utils.Keys.EndpointPolls.*;

/**
 * Created by hp on 2016-07-26.
 */
public class PollsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private RequestQueue requestQueue;
    private TextView textVolleyError;
    private LinearLayout textVolleyErrorLayout;
    View view;
    LinearLayout layout2;

    int pollNumber;
    RadioGroup radioGroup;

    SharedPreferences userDetailsPreference;
    private SwipeRefreshLayout swipeRefreshLayout;

    List<Polls> data = Collections.emptyList();
    List<PollAnswers> dataAnswers = Collections.emptyList();

    private LinearLayout parentLayout;

    public PollsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_polls, container, false);
        parentLayout = (LinearLayout) view.findViewById(R.id.parentLayout);

        //getting the error message layout
        //textVolleyErrorLayout = (LinearLayout) view.findViewById(R.id.textVolleyErrorLayout);
        //getting the error text
        textVolleyError = (TextView) view.findViewById(R.id.textVolleyError);
        //handling the swipe to refresh
        //swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshswipe);
        //swipeRefreshLayout.setOnRefreshListener(this);

        this.view = view;
        sendJsonRequest(view);
        return view;

    }

    private void createLayouts(View view) {

        Log.i("dataSizeforpolls_C", "" + data.size());
        //creating a linear layout
        layout2 = new LinearLayout(getActivity());
        LinearLayout.LayoutParams layout2params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layout2.setLayoutParams(layout2params);
        layout2params.setMargins(0, 0, 0, 10);
        layout2.setOrientation(LinearLayout.VERTICAL);

        layout2.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.pageBackground));


        parentLayout.addView(layout2);

        //creating a radio group

        AppCompatRadioButton radioButton;
        RadioButton radioButton2;
        Button button;
        int order;
        String pollAnswer = "null";
        Log.i("pollAnswers", "" + dataAnswers.size());
        int radioButtonID = 0;
        int radioGroupID = 0;
        //ArrayList<Integer> mapping =
        int[] radioArray = new int[data.size()];
        int[] buttonArray = new int[data.size()];
        ColorStateList colorRadioStateList = new ColorStateList(
                new int[][]{
                        new int[]{ContextCompat.getColor(getActivity(),R.color.textColorPrimary)} //enabled
                },
                new int[] {ContextCompat.getColor(getActivity(),R.color.textColorPrimary) }
        );
        for (int i = 0; i < data.size(); i++) {
            order = -1;
            radioGroup = new RadioGroup(getActivity());
            radioGroup.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            radioGroup.setOrientation(LinearLayout.VERTICAL);
            radioGroup.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.pageBackground));

            //radioGroup.setBackground();
            radioGroup.setPadding(10, 10, 10, 10);
            radioGroupID += 100;
            radioGroup.setId(radioGroupID);
            layout2.addView(radioGroup);


            //creating a textview for the radio group
            TextView textView = new TextView(getActivity());
            textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.textColorPrimary));

            textView.setTextSize(20);
            textView.setPadding(0, 0, 0, 10);
            textView.setText(data.get(i).title);

            //textView.setTextColor(000);
            radioGroup.addView(textView);


            Log.i("data id", "" + data.get(i).id);

            for (int j = 0; j < dataAnswers.size(); j++) {
                Log.i("dataAnswer id", "" + dataAnswers.get(j).poll_id);

                if (data.get(i).id == dataAnswers.get(j).poll_id) {
                    //create the radio buttons


                    radioButton = new AppCompatRadioButton (getActivity());
                    radioButton.setText(dataAnswers.get(j).answer);
                    radioButton.setId(dataAnswers.get(j).poll_options_id);
                    radioButton.setChecked(true);
                    radioButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.textColorPrimary));
                    radioButton.setSupportButtonTintList(colorRadioStateList);
                    //radioButton.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.textColorPrimary));
                    //Log.i("radioID", "" + radioButton.getId());
                    radioGroup.addView(radioButton);
                    radioButtonID++;
                    //radioGroup.setId();
                }
            }
            radioArray[i] = radioGroup.getId();
            pollAnswer = "null";
            order = -1;
            pollNumber = i;
            //getActivity().findViewById(radioGroup.getCheckedRadioButtonId());
            String value = ((RadioButton) getActivity().findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
            //final String value = ((RadioButton) layout2.getRootView().findViewById(radioGroup.getCheckedRadioButtonId() )).getText().toString();
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    //Toast.makeText(getActivity(), "Selected "+checkedId+", Poll ID " +data.get(pollNumber).id, Toast.LENGTH_SHORT).show();
                }
            });


            button = new Button(getActivity());
            button.setText("Send");
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(0, 0, 0, 70);
            button.setLayoutParams(lp);
            button.setTextColor(ContextCompat.getColor(getActivity(), R.color.textColorPrimary));
            button.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //String value = ((RadioButton) getActivity().findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
                    Button b = (Button)v;
                    int indexValue = layout2.indexOfChild(b);
                    RadioGroup radioGroup =  (RadioGroup) getActivity().findViewById(layout2.getChildAt(indexValue-1).getId());
                    int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
                    Toast.makeText(getActivity(),"button clicked "+checkedRadioButtonId, Toast.LENGTH_SHORT).show();
                    sendPoll(checkedRadioButtonId);
                    b.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.divider));
                    b.setEnabled(false);
                }
            });
//        button.setTextColor();
            //button.setId(radioButtonID);
            //buttonArray[i] = radioButtonID;
            layout2.addView(button);

        }

        /*for (int m = 0; m<buttonArray.length;m++){

        }
*/
    }

    private void sendPoll(int pollId) {
        JSONObject jsonObject = new JSONObject();
        Snackbar.make(view, "Sending Poll...", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        userDetailsPreference = getActivity().getSharedPreferences("email", getActivity().MODE_PRIVATE);
        String preferenceValue = userDetailsPreference.getString("id", "nothing");
        try {
            jsonObject.put("poll_option_id", pollId);
            jsonObject.put("user_id", preferenceValue);
            jsonObject.put("longitude", 0);
            jsonObject.put("latitude", 0);

        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://www.mzinda.com/sms/poll-vote", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("Message from server", jsonObject.toString());
                        //dialog.dismiss();
                        //messageText.setText("Image Uploaded Successfully");
                        // Toast.makeText(getApplicationContext(), "Event Created Successfully", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Message from server e", "" + volleyError);
                //dialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jsonObjectRequest);
    }

    @Override
    public void onRefresh() {

        Toast.makeText(getActivity(), "Refreshing", Toast.LENGTH_LONG).show();
        sendJsonRequest(this.view);
    }

    //function for starting the volley request for receiving the report data

    /**
     * Volley method/function that sends a Json request and receives a response
     * Either a JsonObject response or  Volley error response is returned through two anonymous classes
     */
    private void sendJsonRequest(final View view) {
        final ProgressDialog progress = new ProgressDialog(getActivity());
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            //TODO
        } else {
            progress.setTitle("Loading");
            progress.setMessage("Wait while loading...");
            progress.setIndeterminate(true);
            progress.setCanceledOnTouchOutside(true);
            progress.show();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(1), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                textVolleyError.setVisibility(View.GONE);
                data = parseJsonResponse(response);
                Log.i("dataSizeforpolls_i", "" + data.size());
                createLayouts(view);
                /*if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }*/

                progress.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textVolleyError.setVisibility(View.VISIBLE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    textVolleyError.setText(R.string.error_timeout);
                    Log.i("json_stuff", "error-Internet Connection Not Available");
                } else if (error instanceof AuthFailureError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff", "error-Our App failed to connect. Please reload");
                } else if (error instanceof ServerError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff", "error-Our App failed to connect. Please reload");
                } else if (error instanceof NetworkError) {
                    textVolleyError.setText(R.string.error_network);
                    Log.i("json_stuff", "error->Oops! Our Server Just Messed Up");
                } else if (error instanceof ParseError) {
                    textVolleyError.setText(R.string.error_parser);
                    Log.i("json_stuff", "The information came in but had a problem. Please reload");
                }
                progress.hide();
            }
        });
        requestQueue.add(request);
    }

    private ArrayList<Polls> parseJsonResponse(JSONObject response) {
        Log.i("json_stuff", "in the oparseJsonRespose");
        ArrayList<Polls> listCurrentPollss = new ArrayList<>();
        ArrayList<PollAnswers> listCurrentPollsAnswers = new ArrayList<>();
        if (response != null || response.length() > 0) {
            try {
                JSONArray arrayPolls = response.getJSONArray(KEY_POLLS);
                Log.i("pollsArray", "" + arrayPolls);
                //Lets loop through the json array and handle each json object inside
                for (int i = 0; i < arrayPolls.length(); i++) {
                    int id = -1;
                    int pollOptionId = -1;
                    int options_title_order = -1;
                    String Polls = Constants.NA;
                    String start_date = Constants.NA;
                    String end_date = Constants.NA;
                    String title = Constants.NA;
                    String description = Constants.NA;
                    String options_title = Constants.NA;


                    //Get the Polls details of the current json Polls object as we loop
                    JSONObject currentPolls = arrayPolls.getJSONObject(i);
                    //The contains method checks if the key is available and not null, it returns true or false
                    //get the id of the current Polls
                    if (Utilities.contains(currentPolls, KEY_ID)) {
                        id = currentPolls.getInt(KEY_ID);
                    }

                    //get the away team value
                    if (Utilities.contains(currentPolls, KEY_PUBLISHED_DATE)) {
                        start_date = currentPolls.getString(KEY_PUBLISHED_DATE);
                    }
                    //get the away team value
                    if (Utilities.contains(currentPolls, KEY_PUBLISHED_DATE)) {
                        end_date = currentPolls.getString(KEY_PUBLISHED_DATE);
                    }

                    //get the date in theaters for the current movie
                    if (Utilities.contains(currentPolls, KEY_TEXT)) {
                        description = currentPolls.getString(KEY_TEXT);
                    }

                    //get the date in theaters for the current movie
                    if (Utilities.contains(currentPolls, KEY_TITLE)) {
                        title = currentPolls.getString(KEY_TITLE);
                    }

                    //get the date in theaters for the current movie
                    if (Utilities.contains(currentPolls, KEY_POLL_OPTIONS)) {

                        JSONArray arrayPollAnswers = currentPolls.getJSONArray(KEY_POLL_OPTIONS);

                        for (int j = 0; j < arrayPollAnswers.length(); j++) {
                            Log.i("j value", "" + j);
                            Log.i("answers value", "" + arrayPollAnswers.length());
                            //Get the Polls details of the current json Polls object as we loop
                            JSONObject currentPollAnswers = arrayPollAnswers.getJSONObject(j);
                            //get the date in theaters for the current movie
                            if (Utilities.contains(currentPollAnswers, KEY_POLL_OPTIONS_TITLE)) {
                                options_title = currentPollAnswers.getString(KEY_POLL_OPTIONS_TITLE);
                                Log.i("keyoptionsk-->", "" + KEY_POLL_OPTIONS_TITLE);
                                Log.i("keyoptions-->", "" + currentPollAnswers.getString(KEY_POLL_OPTIONS_TITLE));
                            }
                            if (Utilities.contains(currentPollAnswers, KEY_POLL_OPTIONS_TITLE_ORDER)) {
                                options_title_order = currentPollAnswers.getInt(KEY_POLL_OPTIONS_TITLE_ORDER);
                            }
                            if (Utilities.contains(currentPollAnswers, KEY_ID)) {
                                pollOptionId = currentPollAnswers.getInt(KEY_ID);
                            }

                            PollAnswers pollAnswers = new PollAnswers();
                            pollAnswers.answer = options_title;
                            pollAnswers.order = options_title_order;
                            pollAnswers.poll_id = id;
                            pollAnswers.poll_options_id = pollOptionId;
                            listCurrentPollsAnswers.add(pollAnswers);

                        }

                    }

                    /*JSONArray arrayPollAnswers = response.getJSONArray(KEY_POLL_OPTIONS);
                    for (int j = 0; j < arrayPollAnswers.length(); j++) {
                        //get the date in theaters for the current movie
                        if (Utilities.contains(currentPolls, KEY_POLL_OPTIONS_TITLE)) {
                            options_title = currentPolls.getString(KEY_POLL_OPTIONS_TITLE);
                        }
                        if (Utilities.contains(currentPolls, KEY_POLL_OPTIONS_TITLE_ORDER)) {
                             options_title_order = currentPolls.getInt(KEY_POLL_OPTIONS_TITLE_ORDER);
                        }

                        PollAnswers pollAnswers = new PollAnswers();
                        pollAnswers.answer = options_title;
                        pollAnswers.order = options_title_order;
                        pollAnswers.poll_id = id;

                        listCurrentPollsAnswers.add(pollAnswers);

                    }
*/
                    //get the url for the home team thumbnail to be displayed for the home team
                   /* if (Utilities.contains(currentPolls, KEY_)) {
                        imageUrl = currentPolls.getString(KEY_IMAGE);
                    }*/

                    //Create a new Polls(model) object and all the values from the JSON object
                    Polls polls = new Polls();
                    polls.id = id;
                    polls.start_date = start_date;
                    polls.end_date = end_date;
                    polls.description = description;
                    polls.title = title;

                    //asign the poll answers to the dataAnswers array
                    dataAnswers = listCurrentPollsAnswers;
                    //Add the Polls(model/pojo) objects to a list array
                    listCurrentPollss.add(polls);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return listCurrentPollss;
    }

    //function to get the url fort he post data
    public static String getRequestUrl(int page) {

        return UrlEndpoints.MZINDA_POLLS;
    }
}
