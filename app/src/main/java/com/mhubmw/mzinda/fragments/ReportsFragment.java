package com.mhubmw.mzinda.fragments;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.toolbox.Volley;
import com.mhubmw.mzinda.Utils.Constants;

import static com.mhubmw.mzinda.Utils.Keys.EndpointReports.*;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.Utils.UrlEndpoints;
import com.mhubmw.mzinda.Utils.Utilities;
import com.mhubmw.mzinda.adapters.CustomSpinnerAdapter;
import com.mhubmw.mzinda.adapters.ReportsAdapter;
import com.mhubmw.mzinda.models.Comments;
import com.mhubmw.mzinda.models.Report;
import com.mhubmw.mzinda.network.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by hp on 2016-07-26.
 */
public class ReportsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnTouchListener {
    private static final String STATE_REPORTS = "state_reports" ;
    RecyclerView reportRecyclerView;
    ReportsAdapter mReportsAdapter;
    ArrayList<Report> reportList;
    EditText editReport;


    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    LinearLayout imageLinearLayout;
    Button imageCloseButton;
    Button categoryCloseButton;

    LinearLayout photoLinearLayout;
    LinearLayout categoryLinearLayout;
    LinearLayout spinnerlinearlayout;

    SharedPreferences userDetailsPreference;

    private Bitmap bitmap;
    private int PICK_IMAGE_REQUEST = 1;
    private ImageView imageView;
    private boolean imageChecker = false;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private RequestQueue requestQueue;
    private TextView textVolleyError;
    private LinearLayout textVolleyErrorLayout;
    private View mProgressView;
    private ImageView imgPreview;
    private Button imageRemoveButton;

    private SwipeRefreshLayout swipeRefreshLayout;

    //for the image that will be sent on report post
    private String imageLocation = "noLink";

    //for the category selection
    CustomSpinnerAdapter spinnerAdapter;
    //ArrayAdapter<String> spinnerAdapter;
    ArrayList<String> categoryList = new ArrayList<String>();
    JSONObject jsonObject = new JSONObject();

    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;

    private static final String DEBUG_TAG = "Gestures";
    private static final float MIN_SCALE = 0.95f;

    private float mLastScaleFactor = 0;
    private float mTouchY;

    private double lastLongitude;
    private double lastLatitude;

    String userId;
    String userEmail;
    String userTelephone;
    String userLongitude;
    String userLatitude;
    String userIPAddress;

    public ReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_REPORTS, reportList);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        sendJsonRequest();

        /**
         * Handling Location Request
         */
        userDetailsPreference = getActivity().getSharedPreferences("email", getActivity().MODE_PRIVATE);
        String preferenceValue = userDetailsPreference.getString("longitude", "nothing");
        if (preferenceValue.equals("nothing")) {
            ///Toast.makeText(getActivity(),"No Locationd Found", Toast.LENGTH_LONG).show();
            Log.i("mzindaLatLong","No Value Found");
        } else {
            //Toast.makeText(getActivity(),""+preferenceValue, Toast.LENGTH_LONG).show();
            Log.i("mzindaLatLong","Found");
        }
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_reports, container, false);

        //mScaleDetector = new ScaleGestureDetector(getActivity(), (ScaleGestureDetector.OnScaleGestureListener) getActivity());
        /**
         * Setting On touch listener for the zoom functionality
         */
        //view.set(this);
        /**
         * Getting the Phones IP address
         */
        WifiManager wm = (WifiManager) getActivity().getSystemService(getActivity().WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        userDetailsPreference = getActivity().getSharedPreferences("email", getActivity().MODE_PRIVATE);
        userId = userDetailsPreference.getString("id", "nothing");
        userEmail = userDetailsPreference.getString("email", "nothing");
        userTelephone = userDetailsPreference.getString("telephone", "0000000000");
        userLatitude = userDetailsPreference.getString("latitude", "nothing");
        userLongitude = userDetailsPreference.getString("longitude", "nothing");
        userIPAddress = ip;

        Log.i("userInfoID", ""+userId);
        Log.i("userInfoEmail", ""+userEmail);
        Log.i("userInfoIP", ""+userIPAddress);
        //Image in the imageViewer

        imageView = (ImageView) view.findViewById(R.id.selected_img);

        //LinearLayout for image viewer
        imageLinearLayout = (LinearLayout) view.findViewById(R.id.imagelinearlayout);


        //get the button for remove the image and setOnclickListener
        imageRemoveButton = (Button) view.findViewById(R.id.img_remove);
        imageRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Getting the Bitmap from Gallery
                bitmap = null;
                imageLocation = "noLink";
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(null);
                imgPreview.setVisibility(View.GONE);
                imageChecker = false;
                imageLinearLayout.setVisibility(View.GONE);
            }
        });



        //get the button for closing the image and setOnclickListener
        imageCloseButton = (Button) view.findViewById(R.id.img_close);
        imageCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageLinearLayout.setVisibility(View.GONE);
            }
        });

        //get the button for closing the category spinner and setOnclickListener
        categoryCloseButton = (Button) view.findViewById(R.id.categoryclose);
        categoryCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerlinearlayout.setVisibility(View.GONE);
            }
        });
        spinnerAdapter = new CustomSpinnerAdapter(getActivity(), categoryList);
        String[] mTicketArray;
        mTicketArray = getResources().getStringArray(R.array.category_list);
        for (int i = 0; i < mTicketArray.length; i++) {
            categoryList.add(mTicketArray[i]);
        }

        /** Getting a reference to Spinner object of the resource activity_main */
        Spinner spinner = (Spinner) view.findViewById(R.id.categoryspinner);
        //spinner
        /** Setting the adapter to the ListView */
        spinner.setAdapter(spinnerAdapter);

       /* *//** Adding radio buttons for the spinner items *//*
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                final Spinner feedbackSpinner = (Spinner) view.findViewById(R.id.categoryspinner);
                String feedbackType = feedbackSpinner.getSelectedItem().toString();
                //Log.i(LOGTAG, "The selected item from event list is " + feedbackType);
                Log.i("spinner-->", "The selected item from event list is --" + feedbackType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

                final Spinner feedbackSpinner = (Spinner) view.findViewById(R.id.categoryspinner);
                assert feedbackSpinner != null;
                String feedbackType = feedbackSpinner.getSelectedItem().toString();
                Log.i("spinner-->", "The selected item from event list is " + feedbackType);
            }
        });

        //LinerLayout for the photo upload
        photoLinearLayout = (LinearLayout) view.findViewById(R.id.photolayout);
        photoLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageLocation.equals("noLink")) {
                    showFileChooser();

                } else {
                    imageLinearLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        //LinearLayout for the spinner
        spinnerlinearlayout = (LinearLayout) view.findViewById(R.id.spinnerlinearlayout);


        //getting the error message layout
//        textVolleyErrorLayout = (LinearLayout) view.findViewById(R.id.textVolleyErrorLayout);
        //getting the error text
        textVolleyError = (TextView) view.findViewById(R.id.textVolleyError);
        //handling the swipe to refresh
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshswipe);
        swipeRefreshLayout.setOnRefreshListener(this);
        //Handle the recyclerView list
        reportRecyclerView = (RecyclerView) view.findViewById(R.id.reportList);
        mReportsAdapter = new ReportsAdapter(getActivity());
        reportRecyclerView.setAdapter(mReportsAdapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(linearLayoutManager);
        reportRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        editReport = (EditText) view.findViewById(R.id.editReport);

        if(savedInstanceState != null){
            reportList = savedInstanceState.getParcelableArrayList(STATE_REPORTS);
            mReportsAdapter.setReportList(reportList);

        }else{
            sendJsonRequest();
        }




        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            String filePath2 = data.getData().getLastPathSegment();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                imageLocation = filePath.toString();
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(bitmap);
                imageChecker = true;
                //set color to camera icon
                imgPreview = (ImageView) getActivity().findViewById(R.id.img_preview);
                imgPreview.setImageBitmap(bitmap);
                imgPreview.setVisibility(View.VISIBLE);


                //Picasso.with(getActivity()).load(new File(Environment.getExternalStorageDirectory()+"/"+filePath2)).into(imgPreview);
                //Toast.makeText(getActivity(), Environment.getExternalStorageDirectory()+filePath2,Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //function to get the url fort he post data
    public static String getRequestUrl(int page) {

        return UrlEndpoints.MZINDA_REPORTS;
    }
    //function for starting the volley request for receiving the report data

    /**
     * Volley method/function that sends a Json request and receives a response
     * Either a JsonObject response or  Volley error response is returned through two anonymous classes
     */
    private void sendJsonRequest() {
        final ProgressDialog progress = new ProgressDialog(getActivity());
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            //TODO
        } else {
            progress.setTitle("Loading");
            progress.setMessage("Wait while loading...");
            progress.setIndeterminate(true);
            progress.setCanceledOnTouchOutside(true);
            progress.show();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(1), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                textVolleyError.setVisibility(View.GONE);
                reportList = parseJsonResponse(response);

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                    progress.show();
                }

                mReportsAdapter.setReportList(reportList);
                progress.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textVolleyError.setVisibility(View.VISIBLE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    textVolleyError.setText(R.string.error_timeout);
                    Log.i("json_stuff", "error-Internet Connection Not Available");
                } else if (error instanceof AuthFailureError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff", "error-Our App failed to connect. Please reload");
                } else if (error instanceof ServerError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff", "error-Our App failed to connect. Please reload");
                } else if (error instanceof NetworkError) {
                    textVolleyError.setText(R.string.error_network);
                    Log.i("json_stuff", "error->Oops! Our Server Just Messed Up");
                } else if (error instanceof ParseError) {
                    textVolleyError.setText(R.string.error_parser);
                    Log.i("json_stuff", "error->parse error");
                }
                progress.hide();
            }
        });
        requestQueue.add(request);
    }

    private ArrayList<Report> parseJsonResponse(JSONObject response) {
        Log.i("json_stuff", "in the parse place");
        ArrayList<Report> listCurrentReports = new ArrayList<>();
        if (response != null || response.length() > 0) {
            try {
                JSONArray arrayReport = response.getJSONArray(KEY_REPORT);
                //Lets loop through the json array and handle each json object inside
                for (int i = 0; i < arrayReport.length(); i++) {
                    int id = -1;
                    String report = Constants.NA;
                    String date = Constants.NA;
                    String name = Constants.NA;
                    String author = Constants.NA;
                    String commentDescription = Constants.NA;
                    String commentDate = Constants.NA;
                    String verification = Constants.NA;
                    ArrayList<Comments> commentsList = new ArrayList<>();
                    int comments = 0;


                    //Get the Report details of the current json Report object as we loop
                    JSONObject currentReport = arrayReport.getJSONObject(i);
                    //The contains method checks if the key is available and not null, it returns true or false
                    //get the id of the current Report
                    if (Utilities.contains(currentReport, KEY_ID)) {
                        id = currentReport.getInt(KEY_ID);
                    }
                    if (Utilities.contains(currentReport, KEY_ID)) {
                        id = currentReport.getInt(KEY_ID);
                    }
                    //get the home team value
                    if (Utilities.contains(currentReport, KEY_MESSAGE)) {
                        report = currentReport.getString(KEY_MESSAGE);
                    }

                    //get the away team value
                    if (Utilities.contains(currentReport, KEY_DATE)) {
                        date = currentReport.getString(KEY_DATE);
                    }

                    //get the date in theaters for the current movie
                    if (Utilities.contains(currentReport, KEY_COMMENTS)) {
                        //ArrayList<Report> currentCommentList = new ArrayList<>();
                        JSONArray commentsArray = currentReport.getJSONArray(KEY_COMMENTS);
                        for (int j = 0; j < commentsArray.length(); j++) {
                            JSONObject currentComment = commentsArray.getJSONObject(j);
                            if (Utilities.contains(currentComment, KEY_COMMENT_AUTHOR)) {
                                author = currentComment.getString(KEY_COMMENT_AUTHOR);
                                Log.i("comment currentAuthor", "" + author);
                            }

                            if (Utilities.contains(currentComment, KEY_COMMENT_DESCRIPTION)) {
                                commentDescription = currentComment.getString(KEY_COMMENT_DESCRIPTION);
                            }

                            if (Utilities.contains(currentComment, KEY_COMMENT_DATE)) {
                                commentDate = currentComment.getString(KEY_COMMENT_DATE);
                            }
                            comments++;

                            Log.i("comment author", "" + author);
                            Comments comment = new Comments(commentDescription, author, commentDate, -1, id);
                            commentsList.add(comment);

                        }
                    }

                    //

                    //get the date in theaters for the current movie
                    if (Utilities.contains(currentReport, KEY_VERIFICATION)) {
                        verification = currentReport.getString(KEY_VERIFICATION);
                    }

                    //get the url for the home team thumbnail to be displayed for the home team
                    if (Utilities.contains(currentReport, KEY_NAME)) {
                        name = currentReport.getString(KEY_NAME);
                    }

                    //Create a new Report(model) object and all the values from the JSON object
                    Report Report = new Report(id, name, report, date, verification, comments, commentsList);
                    //Report.setId(id);
                    /*Report.report = report;
                    Report.date = date;
                    Report.comments = comments;
                    Report.name = name;
                    Report.verification = verification;*/

                    //Add the Report(model/pojo) objects to a list array
                    listCurrentReports.add(Report);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return listCurrentReports;
    }

    @Override
    public void onRefresh() {
        Toast.makeText(getActivity(), "Refreshing", Toast.LENGTH_LONG).show();
        textVolleyError.setVisibility(View.GONE);
        sendJsonRequest();
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p/>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    public void postReport(View view) {
        String reportMessage = editReport.getText().toString();
        final String reportMessageFail = reportMessage; // if the report fails to send we repopulate the text box with the same message
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                "Sending report. Process will continue in background...", true);

        try {
            jsonObject.put("message", reportMessage);
            jsonObject.put("id", userId);
            jsonObject.put("number", userTelephone);
            jsonObject.put("latitude", userLatitude);
            jsonObject.put("longitude", userLongitude);
            if (imageChecker) {
                Bitmap image = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                Log.i("Bitman bytes", "" + image.getByteCount());
                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                jsonObject.put("image", encodedImage);
            }




            //show loading bar when posting the report
            dialog.show();

            //Set a timer to make dialog disappear after period
            final int interval = 3000; // 1 Second
            Handler handler = new Handler();
            Runnable runnable = new Runnable(){
                public void run() {
                    Toast.makeText(getActivity(), "Sending Report", Toast.LENGTH_SHORT).show();
                }
            };

            Runnable runnable2 = new Runnable(){
                public void run() {
                    //remove loading bar to continue posting in background
                    dialog.dismiss();
                }
            };

            handler.postAtTime(runnable, System.currentTimeMillis()+interval);
            handler.postDelayed(runnable2, interval);


        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlEndpoints.MZINDA_SEND_REPORTS, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("Message from server", jsonObject.toString());
                        dialog.dismiss();//Stop loading bar

                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle("Report Status");
                        alertDialog.setMessage("Report Sent Successfully. Await approval to view it on the platform");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                        //Toast.makeText(getActivity(), "Report Sent Successfully", Toast.LENGTH_SHORT).show();
                        editReport.setText("");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Message from server e", "" + volleyError);
                dialog.dismiss();//Stop loading bar

                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Report Status");
                alertDialog.setMessage("There was a problem submitting your report. Please try again later");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                editReport.setText(reportMessageFail);
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jsonObjectRequest);
        //Toast.makeText(getActivity(), "Sending Report", Toast.LENGTH_LONG).show();
    }



    /*@Override
    public boolean onScale(ScaleGestureDetector detector) {
        return false;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        return false;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {

    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // Let the ScaleGestureDetector inspect all events.
        mScaleDetector.onTouchEvent(ev);
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        super.getActivity().dispatchTouchEvent(ev);
       return onTouchEvent(ev);
    } */

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastScaleFactor = 0;
                mTouchY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                int[] viewCoords = new int[2];
                v.getLocationOnScreen(viewCoords);

                int touchY = (int) event.getY();
                float yDiff = Math.abs(mTouchY - touchY);
                float dragHeightPercentage = yDiff / (float) getView().getHeight();

                mLastScaleFactor = 1 - ((1 - MIN_SCALE) * dragHeightPercentage);

                ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(v, "scaleX", mLastScaleFactor);
                ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(v, "scaleY", mLastScaleFactor);

                scaleDownX.setDuration(0);
                scaleDownY.setDuration(0);

                AnimatorSet scaleDown = new AnimatorSet();
                scaleDown.play(scaleDownX).with(scaleDownY);
                scaleDown.start();
                break;
            case MotionEvent.ACTION_UP:
                ObjectAnimator scaleUpX = ObjectAnimator.ofFloat(v, "scaleX", 1f);
                ObjectAnimator scaleUpY = ObjectAnimator.ofFloat(v, "scaleY", 1f);

                scaleUpX.setDuration(250);
                scaleUpY.setDuration(250);

                AnimatorSet scaleUp = new AnimatorSet();
                scaleUp.play(scaleUpX).with(scaleUpY);
                scaleUp.start();

                mLastScaleFactor = 0;
                mTouchY = 0;
                break;
            default:
                break;
        }
        return false;
    }

    /*---------- Listener class to get coordinates ------------- */
    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            //editLocation.setText("");
            //pb.setVisibility(View.INVISIBLE);
//            Toast.makeText(getActivity(),
// "Location changed: Lat: " + loc.getLatitude() + " Lng: "
//                            + loc.getLongitude(), Toast.LENGTH_SHORT).show();
            String longitude = "Longitude: " + loc.getLongitude();
            Log.v("mzindaLocation", longitude);
            String latitude = "Latitude: " + loc.getLatitude();
            Log.v("mzindaLocation", latitude);
            lastLongitude = loc.getLongitude();
            lastLatitude = loc.getLatitude();

            SharedPreferences.Editor editor = userDetailsPreference.edit();
            editor.putString("latitude", "" + lastLatitude).commit();
            editor.putString("longitude", "" + lastLongitude).commit();
        /*------- To get city name from coordinates -------- */
            String cityName = null;
            Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(loc.getLatitude(),
                        loc.getLongitude(), 1);
                if (addresses.size() > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            String s = longitude + "\n" + latitude + "\n\nMy Current City is: "
                    + cityName;
            //Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            Log.i("mzindaLocation", s);
            //editLocation.setText(s);
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}


    }
}