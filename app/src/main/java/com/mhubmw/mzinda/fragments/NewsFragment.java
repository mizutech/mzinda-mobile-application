package com.mhubmw.mzinda.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.Utils.Constants;
import com.mhubmw.mzinda.Utils.UrlEndpoints;
import com.mhubmw.mzinda.Utils.Utilities;
import com.mhubmw.mzinda.adapters.NewsAdapter;
import com.mhubmw.mzinda.models.News;
import com.mhubmw.mzinda.network.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.mhubmw.mzinda.Utils.Keys.EndpointNews.*;

/**
 * Created by hp on 2016-07-26.
 */
public class NewsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView newsRecyclerView;
    NewsAdapter mNewsAdapter;
    List<News> data;

    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private RequestQueue requestQueue;
    private TextView textVolleyError;
    private LinearLayout textVolleyErrorLayout;
    private View mProgressView;

    private SwipeRefreshLayout swipeRefreshLayout;


    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        sendJsonRequest();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_news, container, false);

        //getting the error message layout
        //textVolleyErrorLayout = (LinearLayout) view.findViewById(R.id.textVolleyErrorLayout);
        //getting the error text
        textVolleyError = (TextView) view.findViewById(R.id.textVolleyError);
        //handling the swipe to refresh
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshswipe);
        swipeRefreshLayout.setOnRefreshListener(this);
        //Handle the recyclerView list

        newsRecyclerView = (RecyclerView) view.findViewById(R.id.newsList);
        mNewsAdapter = new NewsAdapter(getActivity());
        newsRecyclerView.setAdapter(mNewsAdapter);
        newsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        sendJsonRequest();
        return view;
    }

    public List<News> getData(){
        data = new ArrayList<>();
        String[] newsHeadings = {
                "SEWAGES LEAK INTO RIVER",
                "CITIZENS DECRY QUALITY OF WATER",
                "CITIZENS DECRY QUALITY OF WATER",
                "CITIZENS DECRY QUALITY OF WATER",
                "CITIZENS DECRY QUALITY OF WATER",
                "CITIZENS DECRY QUALITY OF WATER",
                "CITIZENS DECRY QUALITY OF WATER",
                "Kuthokoza Chi 2 Ward"

        };

        String[] date = {
                "11:41 Jul 28, 2016",
                "14:18 Jul 27, 2016",
                "17:08 Jul 27, 2016",
                "12:30 Jul 26, 2016",
                "09:21 Jul 25, 2016",
                "05:05 Jul 24, 2016",
                "20:54 Jul 23, 2016",
                "15:15 Jul 20, 2016",
        };

        for (int i = 0;i<8 ;i++){
            News current = new News();
            current.newsHeading = newsHeadings[i];
            current.newsDate = date[i];
            data.add(current);
        }
        return data;
    }

    //function for starting the volley request for receiving the report data

    /**
     * Volley method/function that sends a Json request and receives a response
     * Either a JsonObject response or  Volley error response is returned through two anonymous classes
     */
    private void sendJsonRequest() {
        final ProgressDialog progress = new ProgressDialog(getActivity());
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            //TODO
        } else {
            progress.setTitle("Loading");
            progress.setMessage("Wait while loading...");
            progress.setIndeterminate(true);
            progress.setCanceledOnTouchOutside(true);
            progress.show();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(1), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(textVolleyErrorLayout != null) {
                    textVolleyErrorLayout.setVisibility(View.GONE);
                }
                data = parseJsonResponse(response);

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                progress.hide();
                mNewsAdapter.setdata(data);
                progress.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textVolleyError.setVisibility(View.VISIBLE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    textVolleyError.setText(R.string.error_timeout);
                    Log.i("json_stuff","error-Internet Connection Not Available");
                } else if (error instanceof AuthFailureError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff","error-Our App failed to connect. Please reload");
                } else if (error instanceof ServerError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff","error-Our App failed to connect. Please reload");
                } else if (error instanceof NetworkError) {
                    textVolleyError.setText(R.string.error_network);
                    Log.i("json_stuff","error->Oops! Our Server Just Messed Up");
                } else if (error instanceof ParseError) {
                    textVolleyError.setText(R.string.error_parser);
                    Log.i("json_stuff","error->parse error");
                }
                progress.hide();
            }
        });
        requestQueue.add(request);
    }

    private ArrayList<News> parseJsonResponse(JSONObject response) {
        Log.i("json_stuff","in the parse place");
        ArrayList<News> listCurrentNews = new ArrayList<>();
        if (response != null || response.length() > 0) {
            try {
                JSONArray arrayNews = response.getJSONArray(KEY_NEWS);
                //Lets loop through the json array and handle each json object inside
                for (int i = 0; i < arrayNews.length(); i++) {
                    int id = -1;
                    String news = Constants.NA;
                    String date = Constants.NA;
                    String imageUrl = Constants.NA;
                    String headingUrl = Constants.NA;
                    String newsBody = Constants.NA;
                    String newsExcerpt = Constants.NA;

                    //Get the News details of the current json News object as we loop
                    JSONObject currentNews = arrayNews.getJSONObject(i);
                    //The contains method checks if the key is available and not null, it returns true or false
                    //get the id of the current News
                    if (Utilities.contains(currentNews, KEY_ID)) {
                        id = currentNews.getInt(KEY_ID);
                    }
                    if (Utilities.contains(currentNews, KEY_ID)) {
                        id = currentNews.getInt(KEY_ID);
                    }

                    //get the away team value
                    if (Utilities.contains(currentNews, KEY_PUBLISHED_DATE)) {
                        date = currentNews.getString(KEY_PUBLISHED_DATE);
                    }

                    if (Utilities.contains(currentNews, KEY_EXCERPT)) {
                        newsExcerpt = currentNews.getString(KEY_EXCERPT);
                    }

                    //get the date in theaters for the current movie
                    if (Utilities.contains(currentNews, KEY_TEXT)) {
                        newsBody = currentNews.getString(KEY_TEXT);
                    }

                    //get the date in theaters for the current movie
                    if (Utilities.contains(currentNews, KEY_TITLE)) {
                        headingUrl = currentNews.getString(KEY_TITLE);
                    }

                    //get the url for the home team thumbnail to be displayed for the home team
                    if (Utilities.contains(currentNews, KEY_IMAGE)) {
                        imageUrl = currentNews.getString(KEY_IMAGE);
                    }

                    //Create a new News(model) object and all the values from the JSON object
                    News News = new News();
                    //News.setId(id);
                    News.newsDate = date;
                    News.newsExcerpt = newsExcerpt;
                    News.newsHeading = headingUrl;
                    News.imageUrl = "http://mzinda.com/"+imageUrl;
                    News.newsBody = newsBody;

                    //Add the News(model/pojo) objects to a list array
                    listCurrentNews.add(News);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return listCurrentNews;
    }

    //function to get the url fort he post data
    public static String getRequestUrl(int page) {

        return UrlEndpoints.MZINDA_NEWS;
    }
    @Override
    public void onRefresh() {
        Toast.makeText(getActivity(), "Refreshing", Toast.LENGTH_LONG).show();
        sendJsonRequest();
    }
}
