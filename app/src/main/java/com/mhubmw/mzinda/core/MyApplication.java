package com.mhubmw.mzinda.core;

import android.app.Application;
import android.content.Context;



import com.mhubmw.mzinda.Utils.TypefaceUtil;

/**
 * Created by Noble-mHub on 3/21/2016.
 */
public class MyApplication extends Application {



    private static MyApplication sInstance;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Rubik-Regular.ttf");
        sInstance = this;
    }

    public static Context getContext()
    {
        return MyApplication.context;
    }

    public static MyApplication getInstance(){
        return sInstance;
    }

    public Context getAppContext(){
        return sInstance.getApplicationContext();
    }
}
