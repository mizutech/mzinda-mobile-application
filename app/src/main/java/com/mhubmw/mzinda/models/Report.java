package com.mhubmw.mzinda.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 2016-07-27.
 */
public class Report implements Parcelable {

    public static final Parcelable.Creator<Report> CREATOR
            = new Parcelable.Creator<Report>() {
        public Report createFromParcel(Parcel in) {
            Log.v("Parcel","create from parcel :Report");
            return new Report(in);
        }

        public Report[] newArray(int size) {
            return new Report[size];
        }
    };
    private String name, report, date, verification;
    public int comments, id;


    private ArrayList<Comments> commentsList;

    public Report(){}

    public Report(Parcel input) {
        name = input.readString();
        date = input.readString();
        report = input.readString();
        verification = input.readString();
        comments = input.readInt();
    }


    public Report(int id, String name, String report, String date, String verification, int comments, ArrayList<Comments> commentsList) {
        this.id = id;
        this.name = name;
        this.report = report;
        this.date = date;
        this.verification = verification;
        this.comments = comments;
        this.commentsList = commentsList;
    }

    public ArrayList<Comments> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(ArrayList<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
//        L.m("describe Contents Movie");
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        L.m("writeToParcel Movie");
        dest.writeString(name);
        dest.writeString(date);
        dest.writeString(report);
        dest.writeString(verification);
        dest.writeInt(comments);

    }
}
