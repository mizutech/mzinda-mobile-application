package com.mhubmw.mzinda.models;

/**
 * Created by hp on 2016-08-21.
 */
public class Comments {
    private String comment;
    private String author;
    private String date;
    private int commentId, reportId;

    public Comments(){}

    public Comments(String comment, String author, String date, int commentId, int reportId) {
        this.comment = comment;
        this.author = author;
        this.date = date;
        this.commentId = commentId;
        this.reportId = reportId;
    }




    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }



}
