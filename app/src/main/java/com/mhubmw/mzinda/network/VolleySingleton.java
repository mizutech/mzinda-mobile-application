package com.mhubmw.mzinda.network;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import com.mhubmw.mzinda.core.MyApplication;

/**
 * Created by Noble-mHub on 3/21/2016.
 * Singleton class to restrict volley to one instance across the whole
 *  This means all objects that call Volley will be using the same instance
 *  This is done because instatiating volley multiple times is very expensive on resources
 */
public class VolleySingleton {

    private static VolleySingleton sInstance = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    /**
     *Setting up volley singleton in a constructor that can not be called outside this class
     * We inititate the request queue and image loader objects
     */
    private VolleySingleton()  {
        mRequestQueue = Volley.newRequestQueue(MyApplication.getInstance());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {

            private LruCache<String, Bitmap> cache = new LruCache<>((int)(Runtime.getRuntime().maxMemory()/1024)/8);
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url,bitmap);
            }
        });
    }

    /**
    * Gets the current instance of volley. If it hasn't been instatiated,we instatiate it
    * When instatiated it is return in sInstance field,if the diels is null the singleton hasn't been instatiated
    */
    public static VolleySingleton getInstance(){

         if(sInstance ==null){

             sInstance = new VolleySingleton();
         }

        return sInstance;
    }

    public RequestQueue getRequestQueue(){
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }
}
