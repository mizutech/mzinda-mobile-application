package com.mhubmw.mzinda.Utils;

/**
 * Created by Noble-mHub on 3/21/2016.
 */
public class UrlEndpoints {



    public static final String MZINDA_SEND_REPORTS ="http://www.mzinda.com/sms/reports";

    public static final String MZINDA_SEND_COMMENT ="http://www.mzinda.com/sms/comments";

    public static final String MZINDA_REPORTS ="http://www.mzinda.com/api/reports";

    public static final String MZINDA_USERS ="http://www.mzinda.com/sms/user-login";

    public static final String MZINDA_USERS_GOOGLE ="http://www.mzinda.com/api/google-user";

    public static final String URL_CHAR_QUESTION ="?";

    public static final String URL_CHAR_SLASH ="/";

    public static final String URL_CHAR_AMPERSAND ="&";

    public static final String URL_PARAM_API_KEY ="api_key=";

    public static final String URL_PARAM_PAGE ="limit=";

    public static final String MZINDA_NEWS = "http://mzinda.com/api/news";

    public static final String MZINDA_POLLS = "http://mzinda.com/api/polls";
}
