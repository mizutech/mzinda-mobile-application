package com.mhubmw.mzinda.Utils;

/**
 * Created by Noble-mHub on 3/22/2016.
 * Instead of writing strings in our main classes, we have this interface of keys
 * where keep the string values in variables to maintain the cleanless and readability of code
 */
public interface Keys {


    public interface EndpointNews{

        public static final String KEY_NEWS = "news";
        public static final String KEY_ID = "id";
        public static final String KEY_TITLE = "title";
        public static final String KEY_PUBLISHED_DATE = "published_at";
        public static final String KEY_EXCERPT = "excerpt";
        //public static final String KEY_HEADING = "published";
        public static final String KEY_TEXT = "body";
        public static final String KEY_IMAGE = "image";

    }
    public interface EndpointPolls{

        public static final String KEY_POLLS = "polls";
        public static final String KEY_ID = "id";
        public static final String KEY_TITLE = "title";
        public static final String KEY_PUBLISHED_DATE = "start_date";
        public static final String KEY_END_DATE = "end_date";
        //public static final String KEY_HEADING = "published";
        public static final String KEY_TEXT = "description";
        public static final String KEY_IMAGE = "image";
        public static final String KEY_POLL_OPTIONS = "polloptions";
        public static final String KEY_POLL_OPTIONS_TITLE = "title";
        public static final String KEY_POLL_OPTIONS_TITLE_ORDER = "order";

    }
    public interface EndpointReports{

        public static final String KEY_REPORT = "reports";
        public static final String KEY_MESSAGE = "incident_description";
        public static final String KEY_ID = "id";
        public static final String KEY_COMMENTS = "comments";
        public static final String KEY_COMMENTS_ID = "id";
        public static final String KEY_COMMENT_DESCRIPTION = "comment_description";
        public static final String KEY_COMMENT_AUTHOR = "comment_author";
        public static final String KEY_COMMENT_EMAIL = "comment_email";
        public static final String KEY_COMMENT_DATE = "comment_date";
        public static final String KEY_VERIFICATION = "incident_verified";
        public static final String KEY_NAME = "incident_title";
        public static final String KEY_DATE = "incident_date";


    }



}
