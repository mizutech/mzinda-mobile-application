package com.mhubmw.mzinda.Utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Remote Control on 2016/06/02.
 */
public class PostingTestResults {
    private static final String REGISTER_URL = "http://vitu.diginovamw.com/";


    public Context context;
    JSONArray jsonArray = new JSONArray();
    JSONObject jsonObject = new JSONObject();
    String feedback;
    String email;
    String requestTestID;

    public void sendResults(final String email, final String feedback, Context context) throws JSONException {
        /*final String username = editTextUsername.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();*/
      /*  Map<String, String> jsonParams = new HashMap<String, String>();
        Date date = new Date();
        jsonParams.put(KEY_ID, "2");
        jsonParams.put(KEY_SESSION, "2"+date);
        jsonParams.put(TEST_RESULTS, "PASS");*/
        this.context = context;

        jsonObject = new JSONObject();
        jsonObject.put("email", email);
        jsonObject.put("feedback", feedback);
        jsonArray.put(jsonObject);
        Log.i("dataidobject-->>", "" + jsonObject);
        Log.i("dataidarray-->>", "" + jsonArray);


        this.email = email;
        this.feedback = feedback;
        StringRequest putRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("feedback", feedback);
                Log.i("params-->>", "" + params);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(putRequest);
    }
}
