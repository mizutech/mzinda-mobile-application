package com.mhubmw.mzinda.Utils;

/**
 * Created by Noble-mHub on 3/23/2016.
 */
public interface Constants {
    String NA = "NA";
    public static final String CUSTOM_FONT="fonts/exo2-bold.ttf";
    public static final String CUSTOM_FONT_LIGHT="fonts/exo2-light.ttf";
    public static final String CUSTOM_FONT_REGULAR="fonts/exo2-regular.ttf";
}
