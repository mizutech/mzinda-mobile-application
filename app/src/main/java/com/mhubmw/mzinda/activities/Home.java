package com.mhubmw.mzinda.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.Utils.TabLayoutHelper;
import com.mhubmw.mzinda.fragments.ReportsFragment;
import com.mhubmw.mzinda.fragments.NewsFragment;
import com.mhubmw.mzinda.fragments.PollsFragment;

import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SharedPreferences userDetailsPreference;
    TextView toolbar_title;
    boolean exit = false;
    SparseArray<Integer> ICONS = new SparseArray<>();

    ReportsFragment mReportsFragment;


    private int[] tabIcons = {
            R.drawable.ic_forum_black_24dp,
            R.drawable.ic_edit_black_24dp,
            R.drawable.ic_news
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar_title = (TextView) findViewById(R.id.page_title);


        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        //setupViewPager(viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mReportsFragment = new ReportsFragment(), "ONE");
        adapter.addFragment(new PollsFragment(), "TWO");
        adapter.addFragment(new NewsFragment(), "THREE");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.i("pageSelected->", "" + position);
                getPositionSetToolbarTitle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // customize drawable tint colors the same way as text or use XML attributes
        tabs.setTabTextColors(ContextCompat.getColor(this, R.color.textColorPrimary),ContextCompat.getColor(this, R.color.colorAccent));
        // call this instead of TabLayout#setupWithViewPager
        TabLayoutHelper.setupWithViewPager(tabs, viewPager);

        //We need to get the current item to set up the initial page title
        int initialPosition = viewPager.getCurrentItem();
        Log.i("initialPosition", "" + initialPosition);
        getPositionSetToolbarTitle(initialPosition);


//        tabLayout = (TabLayout) findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(viewPager);
//        setupTabIcons();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mReportsFragment = new ReportsFragment(), "ONE");
        adapter.addFragment(new PollsFragment(), "TWO");
        adapter.addFragment(new NewsFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter implements TabLayoutHelper.IconPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private void fillIcons() {
            ICONS.put(1,R.drawable.ic_forum_black_24dp);
            ICONS.put(2, R.drawable.ic_edit_black_24dp);
            ICONS.put(3, R.drawable.ic_news);
        }

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return mFragmentTitleList.get(position);
            return null;
        }
        @Override
        public int getPageTitleIconRes(int position) {
            // return drawable resource, ICONS here is SparseArray<Integer> mapping position to drawable

            fillIcons();
            return ICONS.get(position);
        }

        @Override
        public Drawable getPageTitleIconDrawable(int position) {
            // overrides getPageTitleIconRes, allows creating drawables on-the-fly
            switch (position){
                case 1:
                    final Drawable drawable = getResources().getDrawable(R.drawable.ic_edit_black_24dp);
                    return drawable;

                case 2:
                    final Drawable drawable2 = getResources().getDrawable(R.drawable.ic_news);
                    return drawable2;

                case 3:
                    final Drawable drawable3 = getResources().getDrawable(R.drawable.ic_edit_black_24dp);
                    return drawable3;

                default:
                    final Drawable drawable1 = getResources().getDrawable(R.drawable.ic_forum_black_24dp);
                    return drawable1;

            }


        }


    }

    public void getPositionSetToolbarTitle(int position) {
        switch (position) {
            case 0:
                toolbar_title.setText("Reports");
                break;
            case 1:
                toolbar_title.setText("Polls");
                break;
            case 2:
                toolbar_title.setText("News");
                break;
            default:
                toolbar_title.setText("Mzinda");
                break;
        }
    }

    public void postReport(View view) {
        mReportsFragment.postReport(view);
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void logout(View view){

        userDetailsPreference = getSharedPreferences("email", MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetailsPreference.edit();
        editor.clear().commit();//Clear the login state in the SharedPrefs

        //Open Login page
        Intent loginIntent = new Intent(this, LoginPage.class);
        startActivity(loginIntent);
    }
}