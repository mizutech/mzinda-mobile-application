package com.mhubmw.mzinda.activities;

import android.app.MediaRouteButton;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.Utils.UrlEndpoints;
import com.mhubmw.mzinda.Utils.Utilities;
import com.google.android.gms.auth.api.Auth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


public class LoginPage extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = "MzindaSignIN";
    EditText userName, password;
    SharedPreferences userDetailsPreference;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private TextView textVolleyError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        userName = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        textVolleyError =(TextView) findViewById(R.id.textVolleyError);

        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]

        findViewById(R.id.sign_in_button).setOnClickListener(this);

        userStatus();
    }

    /**
     * checking if the user has previously logged in f automatic relogin
     */
    private void userStatus() {
        userDetailsPreference = getSharedPreferences("email", MODE_PRIVATE);
        String preferenceValue = userDetailsPreference.getString("id", "nothing");
        if (preferenceValue.equals("nothing")) {
            Log.i("Status", "Not found");
        } else {
            proceed();
        }

    }

    /**
     * Loging into the system with the user credentials
     *
     * @param view
     */
    public void login(View view) {
        boolean userAvailable = false;

        if ((userName.getText().toString().trim() != null && (!(userName.getText().toString().isEmpty())) && (password.getText().toString() != null && (!(password.getText().toString().isEmpty()))))) {
            checkUser(userName.getText().toString(), password.getText().toString(), view);
        }
    }

    /**
     * Checking the user credentials
     *
     * @param username
     * @param password
     * @param view
     * @return
     */
    private boolean checkUser(final String username, String password, final View view) {
        final boolean[] success = {false};
        JSONObject jsonObject = new JSONObject();
        Snackbar.make(view, "Loading...", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        textVolleyError.setVisibility(View.GONE);

        try {
            jsonObject.put("email", username);
            jsonObject.put("password", password);

        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlEndpoints.MZINDA_USERS, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("Message from server", jsonObject.toString());
                        //dialog.dismiss();
                        //messageText.setText("Image Uploaded Successfully");
                        // Toast.makeText(getApplicationContext(), "Event Created Successfully", Toast.LENGTH_SHORT).show();
                        try {
                            if (jsonObject.getBoolean("error")) {
                                //success[0] = true;
                                Log.i("respnse", "" + jsonObject.getBoolean("error"));
                            } else {
                                //Toast.makeText(getApplicationContext(
                                //
                                //
                                // ), "Username and password combination not found", Toast.LENGTH_SHORT).show();

                                Log.i("respnse", "" + jsonObject.getBoolean("error"));
                                //saving data to preferences
                                SharedPreferences.Editor editor = userDetailsPreference.edit();
                                JSONObject objectUserDetails = jsonObject.getJSONObject("user");


                                if (Utilities.contains(objectUserDetails, "id")) {
                                    editor.putString("id", "" + objectUserDetails.getInt("id")).commit();
                                    //id = currentCats.getString(KEY_ID);
                                }
                                if (Utilities.contains(objectUserDetails, "telephone")) {
                                    editor.putString("telephone", "" + objectUserDetails.getInt("telephone")).commit();
                                    //id = currentCats.getString(KEY_ID);
                                }
                                if (Utilities.contains(objectUserDetails, "email")) {
                                    editor.putString("email", "" + objectUserDetails.getString("email")).commit();
                                    //id = currentCats.getString(KEY_ID);
                                }


                                //proceeding to the next activity
                                proceed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                textVolleyError.setVisibility(View.VISIBLE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    textVolleyError.setText(R.string.error_timeout);
                    Log.i("json_stuff","error-Internet Connection Not Available");
                } else if (error instanceof AuthFailureError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff","error-Our App failed to connect. Please reload");
                } else if (error instanceof ServerError) {
                    textVolleyError.setText(R.string.error_auth_failure);
                    Log.i("json_stuff","error-Our App failed to connect. Please reload");
                } else if (error instanceof NetworkError) {
                    textVolleyError.setText(R.string.error_network);
                    Log.i("json_stuff","error->Oops! Our Server Just Messed Up");
                } else if (error instanceof ParseError) {
                    textVolleyError.setText(R.string.error_parser);
                    Log.i("json_stuff","error->parse error");
                }
                Log.e("Server error Message", "" + error);
                //dialog.dismiss();

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
        return success[0];
    }

    private void proceed() {
        Intent homeIntent = new Intent(this, Home.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(homeIntent);
    }

    /**
     * signUp user by redirecting the user to the mzinda sign up page
     *
     * @param view
     */
    public void signUp(View view) {
        Intent signUpIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mzinda.com/auth/register"));
        startActivity(signUpIntent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;

        }
    }
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            /*GoogleSignInAccount acct = result.getSignInAccount();
            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            updateUI(true);*/

            GoogleSignInAccount account = result.getSignInAccount();

            /**
             * Sending google login information to Mzinda Server
             */
            JSONObject jsonObject = new JSONObject();


            try {
                jsonObject.put("id", account.getId());
                jsonObject.put("firstname", account.getGivenName());
                jsonObject.put("lastname", account.getFamilyName());
                jsonObject.put("password", account.getId());

            } catch (JSONException e) {
                Log.e("JSONObject Here", e.toString());
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlEndpoints.MZINDA_USERS, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.e("Message from server", jsonObject.toString());
                            //dialog.dismiss();
                            //messageText.setText("Image Uploaded Successfully");
                            // Toast.makeText(getApplicationContext(), "Event Created Successfully", Toast.LENGTH_SHORT).show();
                            try {
                                if (jsonObject.getBoolean("error")) {
                                    //success[0] = true;
                                    Log.i("respnse", "" + jsonObject.getBoolean("error"));
                                } else {
                                    //Toast.makeText(getApplicationContext(
                                    //
                                    //
                                    // ), "Username and password combination not found", Toast.LENGTH_SHORT).show();

                                    Log.i("respnse", "" + jsonObject.getBoolean("error"));


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("Server error Message", "" + volleyError);
                    //dialog.dismiss();

                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(jsonObjectRequest);


            SharedPreferences.Editor editor = userDetailsPreference.edit();


                editor.putString("id", "" + account.getId()).commit();

                //editor.putString("telephone", "" + ).commit();

                editor.putString("email", "" + account.getEmail()).commit();

            //Toast.makeText(this, "Successful Google Login with the account: "+account.getEmail(), Toast.LENGTH_SHORT).show();
            //proceeding to the next activity
            proceed();
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
            Toast.makeText(this, "Failed Google Login: Make sure you are connected to the internet and you have Google play installed", Toast.LENGTH_SHORT).show();
            Log.i("FAILED GOOGLE", ""+result.getStatus());
        }
    }


   /* private void updateUI(boolean signedIn) {
        if (signedIn) {
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            mStatusTextView.setText(R.string.signed_out);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }*/


}

