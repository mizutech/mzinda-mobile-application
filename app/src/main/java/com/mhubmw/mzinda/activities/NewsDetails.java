package com.mhubmw.mzinda.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.network.VolleySingleton;

public class NewsDetails extends AppCompatActivity {

    private VolleySingleton volleySingleton = VolleySingleton.getInstance();
    private ImageLoader imageLoader;
    private SharedPreferences userDetailsPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();

        setTitle("");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        assert toolbar != null;
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavUtils.navigateUpFromSameTask(NewsDetails.this);
            }
        });


        TextView body = (TextView) findViewById(R.id.body);
        TextView heading = (TextView) findViewById(R.id.heading);
        TextView date = (TextView) findViewById(R.id.date);
        NetworkImageView image = (NetworkImageView) findViewById(R.id.image);
        String imageUrl, newsUrl;

        Bundle newsDetails = getIntent().getExtras();
        assert body != null;
        body.setText(Html.fromHtml(newsDetails.get("body").toString()));
        assert heading != null;
        heading.setText(Html.fromHtml(newsDetails.get("heading").toString()));
        assert date != null;
        date.setText(Html.fromHtml(newsDetails.get("date").toString()));
        imageUrl = newsDetails.get("imageUrl").toString();
        setImage(image, imageUrl);

//        newsUrl = newsDetails.get("newsUrl").toString();


    }

    private void setImage(NetworkImageView image, String imageUrl) {
        if(imageUrl!=null && !imageUrl.isEmpty()){
            image.setImageUrl(imageUrl, imageLoader);
        }
    }

    public void logout(View view){

        userDetailsPreference = getSharedPreferences("email", MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetailsPreference.edit();
        editor.clear().commit();//Clear the login state in the SharedPrefs

        //Open Login page
        Intent loginIntent = new Intent(this, LoginPage.class);
        startActivity(loginIntent);
    }
}
