package com.mhubmw.mzinda.activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mhubmw.mzinda.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginPageFragment extends Fragment {

    public LoginPageFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_page, container, false);
    }
}
