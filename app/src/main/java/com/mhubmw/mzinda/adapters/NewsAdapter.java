package com.mhubmw.mzinda.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.Utils.Constants;
import com.mhubmw.mzinda.activities.NewsDetails;
import com.mhubmw.mzinda.models.News;
import com.mhubmw.mzinda.network.VolleySingleton;

import java.util.Collections;
import java.util.List;

/**
 * Created by hp on 2016-08-05.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    private final LayoutInflater inflator;
    List<News> data = Collections.emptyList();
    private VolleySingleton volleySingleton = VolleySingleton.getInstance();
    private ImageLoader imageLoader;
    Context context;

    public NewsAdapter(Context context) {
        this.inflator = LayoutInflater.from(context);
        this.context = context;
        VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
    }

    @Override
    public int getItemViewType(int position) {
        //Implement your logic here
        News current = data.get(position);
        Log.i("POSITION",""+position);
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = null;

        if(( viewType % 2 == 0)){
            view = inflator.inflate(R.layout.custom_news_right, parent, false);
        }else{
            view = inflator.inflate(R.layout.custom_news, parent, false);
        }

        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final News current = data.get(position);
        this.data = data;
        holder.newsHeading.setText(current.newsHeading);
        holder.newsExcerpt.setText(current.newsExcerpt);
        holder.newsDate.setText(current.newsDate);

        String image = current.imageUrl;
        loadImages(holder, image);
        holder.newsHeading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newsDetailsIntent = new Intent(context, NewsDetails.class);
                newsDetailsIntent.putExtra("heading", current.newsHeading);
                newsDetailsIntent.putExtra("body", current.newsBody);
                newsDetailsIntent.putExtra("date", current.newsDate);
                newsDetailsIntent.putExtra("imageUrl", current.imageUrl);
                newsDetailsIntent.putExtra("newsUrl", current.newsUrl);
                context.startActivity(newsDetailsIntent);
            }
        });
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newsDetailsIntent = new Intent(context, NewsDetails.class);
                newsDetailsIntent.putExtra("heading", current.newsHeading);
                newsDetailsIntent.putExtra("body", current.newsBody);
                newsDetailsIntent.putExtra("date", current.newsDate);
                newsDetailsIntent.putExtra("imageUrl", current.imageUrl);
                newsDetailsIntent.putExtra("newsUrl", current.newsUrl);
                context.startActivity(newsDetailsIntent);
            }
        });

        holder.defaultShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = current.newsUrl;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, current.newsHeading);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

               /* if (intent != null) {
                    // The application exists
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setPackage(application);

                    shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, current.newsHeading);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
                    // Start the specific social application

                    context.startActivity(shareIntent);

                } else {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
                    context.startActivity(Intent.createChooser(share, current.newsHeading));
                }*/
            }
        });
//        holder.twitterShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String application = "com.twitter.android";
//                Intent intent = context.getPackageManager().getLaunchIntentForPackage(application);
//                defaultShare(current.newsUrl, current.newsHeading);
//                /*if (intent != null) {
//                    // The application exists
//                    Intent shareIntent = new Intent();
//                    shareIntent.setAction(Intent.ACTION_SEND);
//                    shareIntent.setPackage(application);
//
//                    shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, current.newsHeading);
//                    shareIntent.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
//                    // Start the specific social application
//                    context.startActivity(shareIntent);
//                } else {
//                    Intent share = new Intent(Intent.ACTION_SEND);
//                    share.setType("text/plain");
//                    share.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
//                    context.startActivity(Intent.createChooser(share, current.newsHeading));
//                }*/
//            }
//        });
//        holder.pintrestShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String application = "com.pinterest";
//                Intent intent = context.getPackageManager().getLaunchIntentForPackage(application);
//
//                defaultShare(current.newsUrl, current.newsHeading);
//                /*if (intent != null) {
//                    // The application exists
//                    Intent shareIntent = new Intent();
//                    shareIntent.setAction(Intent.ACTION_SEND);
//                    shareIntent.setPackage(application);
//
//                    shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, current.newsHeading);
//                    shareIntent.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
//                    // Start the specific social application
//                    context.startActivity(shareIntent);
//                } else {
//                    Intent share = new Intent(Intent.ACTION_SEND);
//                    share.setType("text/plain");
//                    share.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
//                    context.startActivity(Intent.createChooser(share, current.newsHeading));
//                }*/
//            }
//        });
//        holder.googlePlusShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String application = "com.google.android.apps.plus";
//                Intent intent = context.getPackageManager().getLaunchIntentForPackage(application);
//                defaultShare(current.newsUrl, current.newsHeading);
//                /*if (intent != null) {
//                    // The application exists
//                    Intent shareIntent = new Intent();
//                    shareIntent.setAction(Intent.ACTION_SEND);
//                    shareIntent.setPackage(application);
//
//                    shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, current.newsHeading);
//                    shareIntent.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
//                    // Start the specific social application
//                    context.startActivity(shareIntent);
//                } else {
//                    Intent share = new Intent(Intent.ACTION_SEND);
//                    share.setType("text/plain");
//                    share.putExtra(Intent.EXTRA_TEXT, current.newsUrl);
//                    context.startActivity(Intent.createChooser(share, current.newsHeading));
//                }*/
//            }
//        });

    }

    private void defaultShare(String newsUrl, String newsHeading) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, newsUrl);
        context.startActivity(Intent.createChooser(share, newsHeading));
    }

    private void loadImages(final MyViewHolder holder, String image) {
        if (!image.equals(Constants.NA)) {
            holder.image.setImageUrl(image, imageLoader);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setdata(List<News> data) {
        this.data = data;
        notifyItemRangeChanged(0, data.size());
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView newsHeading, newsDate, newsExcerpt;
        NetworkImageView image;
        ImageView facebookShare, twitterShare, pintrestShare, googlePlusShare, defaultShare;

        public MyViewHolder(View itemView) {
            super(itemView);
            newsHeading = (TextView) itemView.findViewById(R.id.news_heading);
            newsExcerpt = (TextView) itemView.findViewById(R.id.news_excerpt);
            newsDate = (TextView) itemView.findViewById(R.id.newsDate);
            image = (NetworkImageView) itemView.findViewById(R.id.news_image);
//            facebookShare = (ImageView) itemView.findViewById(R.id.facebook_share);
//            twitterShare = (ImageView) itemView.findViewById(R.id.twitter_share);
//            pintrestShare = (ImageView) itemView.findViewById(R.id.pintrest_share);
//            googlePlusShare = (ImageView) itemView.findViewById(R.id.google_plus_share);
            defaultShare = (ImageView) itemView.findViewById(R.id.default_share);

        }
    }
}
