package com.mhubmw.mzinda.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.models.Comments;
import com.mhubmw.mzinda.network.VolleySingleton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by hp on 2016-12-14.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    private final LayoutInflater inflator;
    private List<Comments> data = Collections.emptyList();
    private VolleySingleton volleySingleton = VolleySingleton.getInstance();

    private Context context;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public CommentsAdapter(Context context) {
        this.inflator = LayoutInflater.from(context);
        this.context = context;
        VolleySingleton.getInstance();
    }

    @Override
    public CommentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.custom_report_comment, parent, false);
        CommentsAdapter.MyViewHolder holder = new CommentsAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.MyViewHolder holder, int position) {
        final Comments current = data.get(position);
        holder.author.setText(current.getAuthor());
        holder.comment.setText(current.getComment());
        holder.date.setText(timeSince2(current.getDate()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Comments> data) {
        this.data = data;
        notifyItemRangeChanged(0,data.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView author, comment, date;
        public MyViewHolder(View itemView) {
            super(itemView);
            author = (TextView) itemView.findViewById(R.id.author);
            comment = (TextView) itemView.findViewById(R.id.comment);
            date = (TextView) itemView.findViewById(R.id.date);
        }
    }

    public String timeSince(String stringDate) {

        String start_dt = "2016-05-09 18:12:15";
        Log.i("date","date."+stringDate);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = (Date)formatter.parse(start_dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null) {
            Log.i("sentDate", ""+date.toString());
        }

        long time = 0;
        if (date != null) {
            time = date.getTime();
            Log.i("sentDateMill", ""+time);
        }

        long now = new Date().getTime();
        Log.i("cuDateMi", ""+now);
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff =  now - time;
        //diff = 51;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }



    public String timeSince2(String lplp) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = (Date)formatter.parse(lplp);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null) {
            Log.i("sentDate", ""+date.toString());
        }

        long time = 0;
        if (date != null) {
            time = date.getTime();
            Log.i("sentDateMill", ""+time);
        }
        long now = System.currentTimeMillis() % 1000;
        Log.i("cuDateMi", ""+now);
        if (time < now || time <= 0) {
            return null;
        }

        double seconds = Math.floor(( new Date().getTime() - time) / 1000);

        double interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return ((int)interval) + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return ((int)interval) + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return ((int)interval) + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return ((int)interval) + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return ((int)interval) + " minutes ago";
        }
        return  "Just now";
    }

}

