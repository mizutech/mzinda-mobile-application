package com.mhubmw.mzinda.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mhubmw.mzinda.R;
import com.mhubmw.mzinda.Utils.ExpandableTextView;
import com.mhubmw.mzinda.Utils.UrlEndpoints;
import com.mhubmw.mzinda.models.Comments;
import com.mhubmw.mzinda.models.Report;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Comment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by hp on 2016-07-27.
 */
public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.MyViewHolder> {
    private final LayoutInflater inflator;
    private List<Report> data = Collections.emptyList();
    private Context mContext;
    private String commentToSend;
    private RecyclerView commentsRecyclerView;
    private CommentsAdapter mCommentsAdapter;
    private  EditText typedComment;
    SharedPreferences userDetailsPreference;

    public ReportsAdapter(Context context) {

        this.inflator = LayoutInflater.from(context);
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.custom_reports, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Report current = data.get(position);
        holder.name.setText(current.getName());
        holder.date.setText(timeSince2(current.getDate()));
        holder.reportDetails.setText(current.getReport());

        holder.verified.setText(current.getVerification());
      /*  Log.i("VerificationAda->",""+current.verification);*/
        holder.comments.setText("" + current.getComments());
        /**
         * Call the dialog to display the comments fort he report
         */
        holder.viewComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewOther) {
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.myDialog));
                // Get the layout inflater
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View v = inflater.inflate(R.layout.dialog_details, null);
                builder.setView(v);

               typedComment = (EditText) v.findViewById(R.id.typedComment);


                /**
                 * Set the RecyclerView for the comments and also populate the RecyclerView
                 */
                commentsRecyclerView = (RecyclerView) v.findViewById(R.id.commentsList);
                mCommentsAdapter = new CommentsAdapter(mContext);

                mCommentsAdapter.setData(current.getCommentsList());
                commentsRecyclerView.setAdapter(mCommentsAdapter);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                mLayoutManager.setReverseLayout(true);
                mLayoutManager.setStackFromEnd(true);
                commentsRecyclerView.setLayoutManager(mLayoutManager);

                final Button sendComment = (Button) v.findViewById(R.id.sendComment);

                /**
                 * Call the function to send the typed comment to the server when send button is clicked
                 */

                sendComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View vBClick) {
                        Date date = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String newd = sdf.format(date);
                        commentToSend = typedComment.getText().toString();
                        Comments newComment = new Comments(commentToSend, "Vitumbiko Vinkhumbo", newd, -1, current.getId());
                        ArrayList<Comments> commentsList = new ArrayList<Comments>();
                        commentsList = current.getCommentsList();
                        commentsList.add(newComment);


                        mCommentsAdapter.setData(commentsList);

                        postComment(current.getId());
                    }
                });
                /*// Add the button
                builder.setNegativeButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
               */
                // Create the AlertDialog
                AlertDialog dialog = builder.create();

                dialog.show();
            }
        });
        //Setting the verification bar colour
        if ((current.getVerification().equalsIgnoreCase("1"))) {
            holder.verBar.setBackgroundColor(ContextCompat.getColor(mContext ,R.color.verified));
            holder.verified.setTextColor(ContextCompat.getColor(mContext ,R.color.verified));
            holder.verified.setText("Verified");

        } else {
            holder.verBar.setBackgroundColor(ContextCompat.getColor(mContext ,R.color.unverified));
            holder.verified.setTextColor(ContextCompat.getColor(mContext ,R.color.unverified));
            holder.verified.setText("Not Verified");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setReportList(List<Report> reportList) {
        this.data = reportList;
        notifyItemRangeChanged(0, data.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, date, reportDetails, verified, comments;
        LinearLayout verBar;
        LinearLayout viewComments;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            reportDetails = (ExpandableTextView) itemView.findViewById(R.id.report_details);
//            reportDetails.setOnClickListener(new View.OnClickListener() {
//                                                        boolean isCheck = true;
//
//                                                        @Override
//                                                        public void onClick(View view) {
//                                                            if (isCheck) {
//                                                                reportDetails.setMaxLines(10);
//                                                                isCheck = false;
//                                                            } else {
//                                                                reportDetails.setMaxLines(2);
//                                                                isCheck = true;
//                                                            }
//                                                        }
//                                                    });

            verified = (TextView) itemView.findViewById(R.id.verified);
            comments = (TextView) itemView.findViewById(R.id.comments);
            verBar = (LinearLayout) itemView.findViewById(R.id.ver_bar);
            viewComments = (LinearLayout) itemView.findViewById(R.id.commentsButton);
        }
    }
    @Nullable
    private String timeSince2(String lplp) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = (Date)formatter.parse(lplp);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null) {
            Log.i("sentDate", ""+date.toString());
        }

        long time = 0;
        if (date != null) {
            time = date.getTime();
            Log.i("sentDateMill", ""+time);
        }
        long now = System.currentTimeMillis() % 1000;
        Log.i("cuDateMi", ""+now);
        if (time < now || time <= 0) {
            return null;
        }

        double seconds = Math.floor(( new Date().getTime() - time) / 1000);

        double interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return ((int)interval) + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return ((int)interval) + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return ((int)interval) + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return ((int)interval) + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return ((int)interval) + " minutes ago";
        }
        return  "Just now";
    }

    private void postComment(int reportID) {
        String reportMessage = commentToSend;
        WifiManager wm = (WifiManager) mContext.getSystemService(mContext.WIFI_SERVICE);
        String userIPAddress = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        userDetailsPreference = mContext.getSharedPreferences("email", mContext.MODE_PRIVATE);
        String userId = userDetailsPreference.getString("id", "nothing");
        String userEmail = userDetailsPreference.getString("email", "nothing");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("comment", reportMessage);
            jsonObject.put("sender_id", userId);
            jsonObject.put("sender_email", userEmail);
            jsonObject.put("sender_ip", userIPAddress);
            jsonObject.put("report_id", reportID);
        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlEndpoints.MZINDA_SEND_COMMENT, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("Message from server", jsonObject.toString());
                        //dialog.dismiss();
                        //messageText.setText("Image Uploaded Successfully");
                        typedComment.setText("");
                        Toast.makeText(mContext, "Comment Sent Successfully", Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Message from server e", "" + volleyError);
                //dialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(mContext).add(jsonObjectRequest);
        Toast.makeText(mContext, "Sending Report", Toast.LENGTH_LONG).show();
    }





}
